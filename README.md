# BookCatalogue
## Virtual environment creation
- [ ] python -m venv venv

Activate venv using
- [ ] On windows powershell:   ./venv/Scripts/activate

## Install requirements
- [ ] pip install -r requirements.txt
## Database configuration commands:
Run following commands from python shell after activating virual environment
- [ ] from Books import db, create_app, models
- [ ] db.create_all(app=create_app())


## Virtual environment creation
- [ ] python -m venv venv

## Run project
- [ ] Export flask app as afrom activated venv:    $env:FLASK_APP="Books"

- [ ] Run "flask run" command

